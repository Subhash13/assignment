# Pearson Landing Pages Microsite

This project contains all the individual landing pages for pearson's landing page transition initiative.

## Technology stack

This project leverages [Rocket Design System](https://dev.doc.edpl.us/design-system/overview/), an in-house design system project of EdPlus at ASU. It also uses the following technologies:

- NuxtJS Framework
- VueJs
- Bootstrap
- yarn

### Workflow

- Normal workflow, branch out of `develop` to add new feature to the project. The naming convention of feature branch is `feature/[feature-name]`.
- Once your work is done, issue a PR on `develop` branch, and once its approved, it will be passed onto the `staging` env.
- Once its merged onto the `test` branch, then you will be able to test the functionality on [staging environment](https://staging-start.asuonline.asu.edu/).

## Build Setup

```bash
# install dependencies
$ yarn install
# serve with hot reload at localhost:3000
$ yarn dev
# Review production setup in local
$ yarn start
# generate static project /dist is where the files will reside
$ yarn generate
```

## lazy loading for background images image template

```html
<div class="bg-poster" :lazy-background="content.hero.image"></div>
<img v-lazy-load :data-src="content.hero.image" :alt="content.hero.title" />
```

### Adding font awesome to the component

```html
<font-awesome-icon :icon="['fas', 'home']" />
```

## Team

- **Chandi Cumaranatunge** - _Project Lead_ - [Chandi Cumaranatunge](ccumaran@asu.edu)
- **Dipak Purbey** - _Developer_ - [Dipak Purbey](dpurbey@asu.edu)
- **Shashank Shandilya** - _Developer_ - [Shashank Shandilya](sshandi2@asu.edu)
- **Ramya Rao** - _QA_ - [Ramya Rao](rrao9@asu.edu)
- **Amanda Gulley** - _UX | CMS_ - [Amanda Gulley](aholleb@asu.edu)
- **Jeroel Padilla** - _UX | UI Designer_ - [Jeroel Padilla](japadil3@asu.edu)
- **Jonathan Carrol** - _MarTech_ - [Jonathan Carroll](jdcarrol@asu.edu)
- **Sarah Mason** - _SEO_ - [Sarah Mason](smason6@asu.edu)
- **Amir Nourani** - _Data Analytics_ - [Amir Nourani](anourani@asu.edu)

From Team,
Happy coding 🎉🙌

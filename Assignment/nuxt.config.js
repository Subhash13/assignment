export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: "static",

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1, shrink-to-fit=no" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~assets/scss/fonts.scss",
    "~assets/scss/styles.scss",
    "@fortawesome/fontawesome-svg-core/styles.css"
  ],
  server: {
    host: "0.0.0.0" // default: localhost
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins

  plugins: [
    "~/plugins/font-awesome.js",
    "~/plugins/vee-validate.js",
    { src: "~/plugins/vue-carousel.client.js", mode: "client" }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,
  env: {
    NUXT_BASE_LEAD_API_URL: process.env.NUXT_BASE_LEAD_API_URL
  },
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ["@nuxtjs/eslint-module"],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "@nuxt/content", "bootstrap-vue/nuxt", "nuxt-lazy-load"],
  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
    componentPlugins: [
      "FormPlugin",
      "FormGroupPlugin",
      "FormInputPlugin",
      "FormSelectPlugin",
      "ModalPlugin",
      "ImagePlugin",
      "NavbarPlugin",
      "LayoutPlugin",
      "TabsPlugin",
      "EmbedPlugin",
      "CarouselPlugin"
    ],
    directivePlugins: ["VBScrollspyPlugin"]
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
    transpile: ["@rds/asuo-theme",
      "@rds/analytics-gs-mixin",
      "@rds/hero-image-container",
      "@rds/card-image-tile",
      "@rds/card-info-horizontal",
      "@rds/footer-partner",
      "vee-validate",
      "@rds/card-info-vertical",
      "@rds/section-parallax-apollo",
      "@rds/section-parallax-atlas"
    ]
  }
};
